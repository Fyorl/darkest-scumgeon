# Darkest Scumgeon
A Darkest Dungeon save scummer.

## Usage
Run this app alongside your game of Darkest Dungeon. It will automatically make backup saves while you do things in town and will stop making saves when you enter a dungeon. If something bad happens in the dungeon or you simply want to leave without any of the sanity loss or any of your loot, you can simply close the game and restore a previous save.

**Note:** Due to the way the program detects when you enter a dungeon, the most recent save is usually not useable, try restoring the save just before the most recent. If the game complains that your save is corrupted, try restoring an earlier one.
