package uk.me.mantas.darkscum;

import javafx.application.Platform;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.nio.file.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.String.format;
import static java.nio.file.StandardWatchEventKinds.*;

final class SaveMonitor implements Runnable {
	private static final int MAX = 32;
	@Nonnull private static final Logger log = LogManager.getLogger();
	@Nonnull private static final Path storage =
		Paths.get(System.getProperty("java.io.tmpdir"), "darkscum");

	@Nonnull private final DarkestController controller;
	@Nonnull private final Map<WatchKey, Path> keys = new HashMap<>();
	@Nullable private final Path saves;
	@Nullable private final WatchService watcher;
	private boolean inDungeon = false;

	SaveMonitor (@Nonnull final DarkestController controller) {
		controller.setOnRestore(this::restoreSave);
		this.controller = controller;
		saves = detectSaveDirectory();

		WatchService watcher = null;
		if (saves != null) {
			try {
				watcher = saves.getFileSystem().newWatchService();
			} catch (final IOException e) {
				log.error("Failed to create WatchService.", e);
			}
		}

		this.watcher = watcher;
	}

	@Override
	public void run () {
		backupAllSaves();
		cleanup();
		updateList();

		if (saves == null || watcher == null) {
			updateStatus("Failed...");
			return;
		}

		try (final Stream<Path> files = Files.list(saves)) {
			files.filter(Files::isDirectory).forEach(this::register);
		} catch (final IOException e) {
			log.error("Failed to begin monitoring.", e);
			updateStatus("Failed...");
		}

		while (true) {
			WatchKey key;
			try {
				updateStatus(inDungeon ? "In dungeon..." : "Monitoring...");
				key = watcher.take();
			} catch (final InterruptedException e) {
				break;
			}

			final boolean requiresBackup = key.pollEvents().stream().anyMatch(this::filterEvent);
			if (requiresBackup) {
				backupSave(keys.get(key));
				cleanup();
				updateList();
			}

			if (!key.reset()) {
				log.warn("A directory became unavailable for monitoring.");
			}
		}
	}

	private void backupAllSaves () {
		updateStatus("Backing up saves...");
		if (saves == null) {
			return;
		}

		try (final Stream<Path> files = Files.list(saves)) {
			files.filter(Files::isDirectory).forEach(this::backupSave);
		} catch (final IOException e) {
			log.error("Failed to backup saves.", e);
		}
	}

	private void backupSave (@Nonnull final Path profile) {
		try {
			final Path backupProfile = storage.resolve(backupName(profile.getFileName()));
			Files.createDirectories(backupProfile);
			Files.walk(profile)
				.filter(path -> path.getParent().equals(profile))
				.filter(Files::isRegularFile)
				.forEach(from -> copyFile(from, backupProfile));
		} catch (final IOException e) {
			log.error(format("Failed to backup '%s'.", profile), e);
		}
	}

	private void cleanup () {
		updateStatus("Cleaning up old backups...");
		if (!Files.exists(storage)) {
			return;
		}

		try (final Stream<Path> files = Files.list(storage)) {
			final List<Path> backups =
				files.sorted(storageComparator(SortDirection.ASCENDING))
					.collect(Collectors.toList());

			final int purge = backups.size() - MAX;
			for (int i = 0; i < purge; i++) {
				delete(backups.get(i));
			}
		} catch (final IOException e) {
			log.error("Failed to clean up old backups.", e);
		}
	}

	@SuppressWarnings("unchecked")
	private boolean filterEvent (@Nonnull final WatchEvent<?> event) {
		final WatchEvent.Kind<?> kind = event.kind();
		if (kind == OVERFLOW) {
			log.warn("Watch event overflowed.");
			return false;
		}

		final WatchEvent<Path> pathEvent = (WatchEvent<Path>) event;
		final Path path = pathEvent.context();
		final String filename = path.getFileName().toString();
		log.info(format("%s: %s", kind, path));

		if (kind == ENTRY_DELETE && filename.equals("persist.raid.json")) {
			inDungeon = false;
			return false;
		}

		if (inDungeon) {
			return false;
		}

		if (kind == ENTRY_CREATE && filename.equals("persist.raid.json")) {
			inDungeon = true;
			return false;
		}

		//noinspection RedundantIfStatement
		if (kind == ENTRY_CREATE && filename.equals("persist.game.json")) {
			return true;
		}

		return false;
	}

	private void register (@Nonnull final Path path) {
		try {
			keys.put(path.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY), path);
		} catch (final IOException e) {
			log.error(format("Failed to watch '%s'.", path), e);
		}
	}

	private void restoreSave (@Nonnull final Backup backup) {
		if (saves == null) {
			return;
		}

		final Path profile = saves.resolve(backup.nameProperty().get());
		final Path restoreFrom = storage.resolve(backup.toString());
		delete(profile);

		try {
			Files.createDirectories(profile);
			try (final Stream<Path> files = Files.list(restoreFrom)) {
				files.forEach(file -> copyFile(file, profile));
			}

			register(profile);
			return;
		} catch (final IOException e) {
			log.error(format("Failed to create profile directory '%s'.", profile), e);
		}

		Platform.runLater(() -> controller.restoreFailed(storage, saves));
	}

	private void updateList () {
		if (!Files.exists(storage)) {
			return;
		}

		try (final Stream<Path> files = Files.list(storage)) {
			final List<Backup> backups =
				files.sorted(storageComparator(SortDirection.DESCENDING))
					.map(Backup::fromPath)
					.collect(Collectors.toList());

			Platform.runLater(() -> {
				controller.list.getItems().clear();
				controller.list.getItems().addAll(backups);
			});
		} catch (final IOException e) {
			log.error("Failed to update display.", e);
		}
	}

	private void updateStatus (@Nonnull final String msg) {
		Platform.runLater(() -> controller.status.setText(format("Status: %s", msg)));
	}

	@Nonnull
	private static String backupName (@Nonnull final Path originalName) {
		return format("%d-%s", System.currentTimeMillis(), originalName);
	}

	private static void copyFile (@Nonnull final Path from, @Nonnull final Path into) {
		final Path to = into.resolve(from.getFileName());
		try {
			Files.copy(from, to);
		} catch (final IOException e) {
			log.error(format("Failed to copy '%s' -> '%s'.", from, to), e);
		}
	}

	private static void delete (@Nonnull final Path path) {
		try {
			if (Files.isDirectory(path)) {
				try (final Stream<Path> files = Files.list(path)) {
					files.forEach(SaveMonitor::delete);
				}
			}
			Files.delete(path);
		} catch (final IOException e) {
			log.error(format("Failed to delete '%s'.", path), e);
		}
	}

	@Nullable
	private static Path detectSaveDirectory () {
		final Path userdata;
		if (System.getenv("ProgramFiles(x86)") != null) {
			userdata = Paths.get(System.getenv("ProgramFiles(x86)"), "Steam", "userdata");
		} else if (System.getenv("ProgramFiles") != null) {
			userdata = Paths.get(System.getenv("ProgramFiles"), "Steam", "userdata");
		} else if (System.getenv("HOME") != null) {
			userdata = Paths.get(System.getenv("HOME"), ".steam", "steam", "userdata");
		} else {
			log.error("Failed to detect Darkest Dungeon save directory: Could not find userdata.");
			return null;
		}

		try (final Stream<Path> files = Files.list(userdata)) {
			final Optional<Path> userID = files.findFirst();
			if (!userID.isPresent()) {
				log.error(
					"Failed to detect Darkest Dungeon save directory: Could not find user ID.");
				return null;
			}

			final Path saves = userdata.resolve(userID.get()).resolve("262060").resolve("remote");
			if (!Files.exists(saves)) {
				log.error("Darkest Dungeon save directory did not exist.");
				return null;
			}

			return saves;
		} catch (final IOException e) {
			log.error("Failed to detect Darkest Dungeon save directory.", e);
		}

		return null;
	}

	private enum SortDirection {ASCENDING, DESCENDING}

	@Nonnull
	private static Comparator<? super Path> storageComparator (
		@Nonnull final SortDirection direction)
	{
		final int multiplier = direction == SortDirection.ASCENDING ? 1 : -1;
		return (a, b) -> {
			final String aName = a.getFileName().toString();
			final String bName = b.getFileName().toString();
			try {
				final long aStamp = Long.parseLong(aName.substring(0, aName.indexOf('-')));
				final long bStamp = Long.parseLong(bName.substring(0, bName.indexOf('-')));
				return Long.compare(aStamp, bStamp) * multiplier;
			} catch (final NumberFormatException e) {
				log.error(format("Badly formatted backup ('%s' / '%s').", aName, bName), e);
			}

			return 0;
		};
	}
}
