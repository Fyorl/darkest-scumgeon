package uk.me.mantas.darkscum;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Darkest extends Application {
	@Nonnull private static final Logger log = LogManager.getLogger();
	@Nonnull private static final ExecutorService monitorThread =
		Executors.newSingleThreadExecutor();

	public static void main (@Nonnull final String[] args) {
		launch(args);
	}

	@Override
	public void start (@Nonnull final Stage stage) {
		try {
			final FXMLLoader loader = new FXMLLoader(getClass().getResource("/darkest.fxml"));
			final Parent root = loader.load();
			final Scene scene = new Scene(root);
			stage.setTitle("Darkest Scumgeon");
			stage.setScene(scene);
			stage.show();

			final DarkestController controller = loader.getController();
			final SaveMonitor monitor = new SaveMonitor(controller);
			monitorThread.submit(monitor);
			stage.setOnCloseRequest(x -> monitorThread.shutdownNow());
		} catch (final IOException e) {
			log.error("Failed to load FXML.", e);
		}
	}
}
