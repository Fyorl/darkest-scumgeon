package uk.me.mantas.darkscum;

import javafx.scene.control.ListCell;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.DateTimeException;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import static java.lang.String.format;

public class BackupCell extends ListCell<Backup> {
	@Nonnull private static final Logger log = LogManager.getLogger();
	@Nonnull private static final DateTimeFormatter dateFormatter =
		DateTimeFormatter.ofPattern("EEE d MMM, HH:mm:ss", Locale.UK).withZone(ZoneOffset.UTC);

	@Override
	public void updateItem (@Nullable final Backup item, final boolean empty) {
		super.updateItem(item, empty);
		if (item == null || empty) {
			return;
		}

		try {
			setText(
				format(
					"%s %s",
					dateFormatter.format(Instant.ofEpochMilli(item.timestampProperty().get())),
					item.nameProperty().get()));
		} catch (final DateTimeException e) {
			log.error(e);
		}
	}
}
