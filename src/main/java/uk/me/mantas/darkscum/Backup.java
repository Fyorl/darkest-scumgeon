package uk.me.mantas.darkscum;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.nio.file.Path;

import static java.lang.String.format;

final class Backup {
	@Nonnull private static final Logger log = LogManager.getLogger();
	@Nonnull private final LongProperty timestamp = new SimpleLongProperty();
	@Nonnull private final StringProperty name = new SimpleStringProperty();

	@Nullable
	static Backup fromPath (@Nonnull final Path path) {
		final String filename = path.getFileName().toString();
		final int dash = filename.indexOf('-');

		try {
			final long timestamp = Long.parseLong(filename.substring(0, dash));
			final String name = filename.substring(dash + 1);
			return new Backup(timestamp, name);
		} catch (final NumberFormatException e) {
			log.error(format("Corrupt backup name '%s'.", filename), e);
		}

		return null;
	}

	@Nonnull
	StringProperty nameProperty () {
		return name;
	}

	@Nonnull
	LongProperty timestampProperty () {
		return timestamp;
	}

	private Backup (final long timestamp, @Nonnull final String name) {
		this.timestamp.set(timestamp);
		this.name.set(name);
	}

	@Nonnull
	@Override
	public String toString () {
		return format("%d-%s", timestamp.get(), name.get());
	}
}
