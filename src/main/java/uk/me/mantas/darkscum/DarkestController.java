package uk.me.mantas.darkscum;

import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.nio.file.Path;
import java.util.function.Consumer;

import static java.lang.String.format;

public final class DarkestController {
	@FXML public Label status;
	@FXML public ListView<Backup> list;
	@FXML public Button restore;

	@Nullable private Alert confirmation;
	@Nullable private Alert error;
	@Nullable private Consumer<Backup> onRestore;

	void setOnRestore (@Nonnull final Consumer<Backup> onRestore) {
		this.onRestore = onRestore;
	}

	void restoreFailed (
		@SuppressWarnings("SameParameterValue") @Nonnull final Path storage,
		@Nonnull final Path saves)
	{
		if (error == null) {
			return;
		}

		error.setContentText(
			format(
				"Restore failed, you will need to manually copy the files yourself from %s to %s",
				storage,
				saves));

		error.showAndWait();
	}

	@FXML
	public void initialize () {
		confirmation =
			new Alert(
				AlertType.WARNING,
				"Make sure Darkest Dungeon is closed before proceeding.",
				ButtonType.OK,
				ButtonType.CANCEL);

		error = new Alert(AlertType.ERROR, "", ButtonType.OK);
		confirmation.setTitle("Warning");
		error.setTitle("Error");
		list.setCellFactory(x -> new BackupCell());
		restore.disableProperty().bind(
			Bindings.isEmpty(list.getSelectionModel().getSelectedIndices()));
	}

	@FXML
	public void restoreSave () {
		if (confirmation == null) {
			return;
		}

		confirmation.showAndWait();
		if (confirmation.getResult() == ButtonType.CANCEL) {
			return;
		}

		final Backup restore = list.getSelectionModel().getSelectedItem();
		if (restore == null || onRestore == null) {
			return;
		}

		onRestore.accept(restore);
	}
}
